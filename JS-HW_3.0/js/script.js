
// v1.10 (CW)
// создаем массив
const arr = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15];
// спрашиваем у пользователя какой элемент массива удалить
const del = prompt('Выберите индекс элемента массива, подлежащий удалению');
// Разворачиваем элементы в массиве
arr.reverse();
// Удаляем выбраный пользователем элемант
arr.splice(del - 1, 1);
// Выводим в консоль массив и в документ
console.log(arr);
document.write(arr.join(' &#9729; '));
// v1.10 (CW) END

// v1.11 (HW)
// Создаем массив
const styles = ['Джаз', 'Блюз'];
// Добаляем элемент в конец массива
styles.push('Рок-н-ролл');
console.log(styles);
// Заменяем ценртальный элемент массива
styles.splice((styles.length / 2), 1, 'Классика');
console.log(styles);
// Удаляем первый элемент массива и выводим его в консоль
const firstObjectArray = styles.shift();
console.log(firstObjectArray);
// Добавляем элементы в начало массива
styles.splice(0, 0, 'Рэп', 'Регги');
console.log(styles);
// v1.11 (HW) END

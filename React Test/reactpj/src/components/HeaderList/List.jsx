import React from 'react'

const List = () => {
    return (
        <ul>
            <li>Home</li>
            <li>PhotoApp</li>
            <li>Design</li>
            <li>Download</li>
        </ul>
    )
}

export default List;
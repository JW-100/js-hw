import React from 'react'
import Container from '../../Container'

const NavigationEl3 = () => {
    return (
        <div className='nav__column'>
            <a className='nav__item' href="#">Design</a>
        </div>
    )
}

export default NavigationEl3;
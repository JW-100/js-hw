import React from 'react'
import Container from '../../Container'

const NavigationEl2 = () => {
    return (
        <div className='nav__column'>
            <a className='nav__item' href="#">Photoapp</a>
        </div>
    )
}

export default NavigationEl2;
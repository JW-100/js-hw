import React from 'react'
import Container from '../../Container'

const NavigationEl1 = () => {
    return (
        <div className='nav__column'>
            <a className='nav__item' href="#">Home</a>
        </div>
    )
}

export default NavigationEl1;
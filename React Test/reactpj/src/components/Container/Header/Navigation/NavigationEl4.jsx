import React from 'react'
import Container from '../../Container'

const NavigationEl4 = () => {
    return (
        <div className='nav__column'>
            <a className='nav__item' href="#">Download</a>
        </div>
    )
}

export default NavigationEl4;
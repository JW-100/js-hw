import React from 'react'
import NavigationEl1 from './Navigation/NavigationEl1'
import NavigationEl2 from './Navigation/NavigationEl2'
import NavigationEl3 from './Navigation/NavigationEl3'
import NavigationEl4 from './Navigation/NavigationEl4'

const Header = () => {
    return (
        <header className = 'nav'>
            <NavigationEl1/>
            <NavigationEl2/>
            <NavigationEl3/>
            <NavigationEl4/>
        </header>
    )
}

export default Header;
import React from 'react'

const Footer = () => {
    return (
        <footer>
            <span class="legal">Copyright by phototime - all right reserved</span>
        </footer>
    )
}

export default Footer;
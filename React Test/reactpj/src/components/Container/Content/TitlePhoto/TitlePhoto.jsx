import React from 'react'

const TitlePhoto = () => {
    return (
        <img src="./img/top-img.png" alt="Camera" title="Camera"></img>
    )
}

export default TitlePhoto;
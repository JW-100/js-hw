import React from 'react'
import ButtonEl from './ButtonEl/ButtonEl'

const Button = () => {
    return ( 
        <div className='button'>
            <ButtonEl/>
        </div>
    )
}

export default Button;
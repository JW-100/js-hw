import React from 'react'
import Button from '../Button'

const ButtonEl = () => {
    return (
        <button>
            <a class="button__link" href="https://cameralabs.org/2938-fotografiya-kak-iskusstvo-35-neveroyatno-krasivich-portretov">get started</a>
        </button>
    )
}

export default ButtonEl;
import React from 'react'
import DescriptPart1 from './DescriptParts/DescriptPart1'
import DescriptPart2 from './DescriptParts/DescriptPart2'

const Description = () => {
    return (
        <div className='description'>
            <DescriptPart1/>
            <DescriptPart2/>
        </div>
    )
}

export default Description;
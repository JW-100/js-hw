import React from 'react'
import Button from './Button/Button'
import Description from './Description/Description'
import MainTitle from './MainTitle/MainTitle'
import TitlePhoto from './TitlePhoto/TitlePhoto'

const Content = () => {
    return (
        <main className='content'>
            <TitlePhoto/>
            <MainTitle/>
            <Description/>
            <Button/>
        </main>
    )
}

export default Content;
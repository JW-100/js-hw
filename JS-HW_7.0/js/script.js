
//QUEST 7.1
const words = [
    "программа",
    "макака",
    "прекрасный",
    "оладушек",
    "офис",
    "разметка"
];
// выбираем рандомное слово
const word = words[Math.floor(Math.random() * words.length)];
// создаем итоговый массив
const answerArray = [];
for (let i = 0; i < word.length; i++) {
    answerArray[i] = "_";
};

let remainingLetters = word.length;
// игровой цикл
while (remainingLetters > 0) {
    alert(answerArray.join(""));
    // Запрашивает вариант ответа
    let guess = prompt('Угадайте букву, или нажмите Отмена для выхода из игры.');
    if (guess === null) {
        // Выходим из игрового цикла
        break;
    } else if (guess.length !== 1) {
        alert("Пожалуйста, введите одиночную букву.");
    } else {
        // Обновляем состояние игры
        for (let j = 0; j < word.length; j++) {
            if (word[j] === guess) {
                answerArray[j] = guess;
                remainingLetters--;
            }
        }
    }
};
// Конец игрового цикла
// Отображаем ответ и поздравляем игрока
alert(answerArray.join(" "));
alert(`Отлично! Было загадано слово ${word}`);
// QUEST 7.1 (END)

// HW 7.1 
// Функция-конструктор
function createNewUser (firstName, lastName) {
    this.firstName = prompt('Введите ваше имя'),
    this.lastName = prompt('Укажети вашу фамилию'),
    this.birthday = prompt('Укажите дату рождения в формате dd.mm.yyyy')
    // метод будет генерировать Логин посредсвом склейки первой буквы имени и фамилии строчными буквами
    this.getLogin = function(){
        let login = this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase();
        return login; 
    };    
    // считает возраст
    this.getAge = function(birthday) {
        let today = new Date();
        let birthDate = new Date(this.birthday);
        let age = today.getFullYear() - birthDate.getFullYear();
        let month = today.getMonth() - birthDate.getMonth();
        if ( month < 0 || ( month === 0 && today.getDate() < birthDate.getDate())) {
            age = age - 1;
        }
        return age;
    };
    // генерирует пароль посредством склейки первой буквы имени (заглавная) + фамилия строчными и + год рождения
    this.getPassword = function (birthday) {
        let password = this.firstName.charAt(0).toUpperCase() + this.lastName.toLowerCase() + this.birthday.substring(6,10);
        return password;
    }
};

const newUser = new createNewUser;
alert(`Your login is ${newUser.getLogin()}`);
alert(`You're ${newUser.getAge()} years old`);
alert(`Your PASS is ${newUser.getPassword()}`);
// HW 7.1 (END)

// HW 7.2
const arr = ['hello', 'word', 23, '23', null, undefined];
// Фильтрует массив и возвращает новый
const filterBy = (arr, type) => arr.filter(item => typeof item !== type);

console.log(filterBy(arr, 'string'));
// HW 7.2 (END)


// v.1.12 (CW)
const criptoWallet = {
    walletHoder: 'Unknown',
    Bitcoin: {
        name: 'Bitcoin',
        logo: '&#8383;',
        balance: 24,
        price: 10695.88
    },
    Ethereum: {
        name: 'Ethereum',
        logo: '&#165;',
        balance: 13,
        price: 378.37
    },
    Stellar: {
        name: 'Stellar',
        logo: '&#8361;',
        balance: 7800,
        price: 0.081420
    },
    show : function (cc) {
        document.write(`Добрый день, ${this.walletHoder}! Ваш баланс по состоянию на 22/09/2020 ${this[cc].name} ${this[cc].logo } 
        состовляет ${this[cc].balance}. Вы можете их продать и получить ${this[cc].price * this[cc].balance * 28.19} UAH &#8372 <hr>`);
    }
};

const currency1 = 'Bitcoin';
const currency2 = 'Ethereum'
const currency3 = 'Stellar'

criptoWallet.show(currency1);
criptoWallet.show(currency2);
criptoWallet.show(currency3);

// v.1.12 (CW) END

// v.1.13 (HW)
const obj = {
    title : 'About',
    body : 'body',
    footer : 'footer',
    date : 'September 30 2020',
    application : {
        title1 : {
            val : 'PALANTIR Technologies'
        },
        body1 : {
            val : 'Palantir — частная американская компания, разработчик программного обеспечения анализа данных для организаций, основные заказчики — спецслужбы, инвестиционные банки, хедж-фонды.'
        },
        footer1 : {
            val : 'About'
        },
        date1 : {
           val : 'September 2020'
        }
    },
    show2 : function (zl) {
        document.write(`Вот что мне удалось найти по вашему запросу:)) <br> ${this.application[zl].val} <br><br>`)
    }
}

const r = `title1`,
      l = `body1`;


obj.show2(r);
obj.show2(l);


// Функция будет обрабатывать массив при помощи метода ForEach и с помощью метода Push будет добавлять по элементу в новый массив.
// Вернет новый массив result копию массива array
function map(fn, array) {
    let result = [];
    array.forEach(function(item) {
        result.push(fn(item))
    });
    return result;
};

const array = ['P', 'A', 'L', 'A', 'N', 'T', 'I', 'R'];

function func(element) {
    return element + '1'
};

console.log(array);
console.log(map(func, array));

// Part 2

function checkAge (age) {
    return age > 18 ? (true) : (confirm('Родители разрешили?'));
};


// Part 3 (CW)
function add(a, b) {
    if (a === undefined) {
        a = 0;
    };
    if (b === undefined) {
        b = 0;
    }
    return a + b;
};

function subtract(a, b) {
    if (a === undefined) {
        a = 0;
    };
    if (b === undefined) {
        b = 0;
    }
    return a - b;
};

function multiple(a, b) {
    if (a === undefined) {
        a = 0;
    };
    if (b === undefined) {
        b = 0;
    }
    return a * b;
};

function devide(a, b) {
    if (a === undefined) {
        a = 0;
    };
    if (b === undefined) {
        b = 0;
    }
    if (b == 0) {
        return alert('Erorr');
    }
    return a / b;
};

function calculate(a, b, fn) {
    if (b == 0) {
        return alert('Error')
    };
    return fn(a, b);
}

